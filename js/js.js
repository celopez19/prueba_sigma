$(document).ready(function(){
	
	var data_deptos='';
	var arr_deptos=[];
	var arr_munpos=[];
	
	$.getJSON('https://api.npoint.io/7b5ffc26aa57221ea218', function(data){
		//console.log(data);
		data_deptos=data;
		
		Object.keys(data_deptos).forEach(function(key) {
		  //console.table('Key : ' + key + ', Value : ' + data[key]);
		  arr_deptos.push(key);
		});
		
		if(arr_deptos.length>0){
			arr_deptos.sort();
			arr_deptos.forEach(function(depto){
				$('#departamento').append('<option value="'+depto+'">'+depto+'</option>');
			});
			
			$('#departamento').change(function(){
				valor = $(this).val();
				
				for (const prop in data_deptos) {
				  if(prop==valor){
					//console.log(data_deptos[prop].length);
					$('#ciudad option').remove();
					$('#ciudad').append('<option value="">Seleccionar</option>');
					data_deptos[prop].forEach(function(munpo){
						$('#ciudad').append('<option value="'+munpo+'">'+munpo+'</option>');
					});
				  }
				}
				/*$.each(data_deptos.valor, function(x){
					$('#ciudad').append('<option value="'+data_deptos.valor[x]+'">'+data_deptos.valor[x]+'</option>');
				});*/
				
				/*Object.keys(data_deptos).forEach(function(key) {
				  if(valor==key){ 
					//arr_munpos.push(data[key]);
					$('#ciudad').append('<option value="'+data[key]+'">'+data[key]+'</option>');
				  }
				  
				});*/
				
				/*if(arr_munpos.length>0){
					arr_munpos.sort();
					arr_munpos.forEach(function(munpo){
						$('#ciudad').append('<option value="'+munpo+'">'+munpo+'</option>');
					});
				}*/
			});
		}
	});
});

function sendForm(){
	var formData = new FormData($("#frm_sigma")[0]);
	$.ajax({
		url:'db/contacts.php',
		type:"POST",
		data:formData,
		processData: false,
		contentType: false,
		success:function(res){
			res = JSON.parse(res);
			
			if(res.res==1){ 
				$("#modal_registro").modal("show");
				location.reload();
		    }else alert('Ha ocurido un error, asegúrate de completar todos los campos');
		}
	});
}